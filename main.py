import streamlit as st
import pandas as pd
import numpy as np
import pandas as pd


from sklearn.tree import DecisionTreeClassifier


import datetime
st.title('__Crypto Trend Prediction__')
st.text("The dataset used for Demo contains data till November 30, 2022")
st.text("Please select 7 days interval to achieve the best Accuracy")




def load_data():
    d1 = st.date_input(
    "Enter The Start Date(Must be after 2022/06/21)",
    datetime.date(2022, 6, 21))
    st.write('Start Date', d1)
    d2 = st.date_input(
    "Enter The End Date(Must be before 2022/11/30)",
    datetime.date(2022, 6, 27))
    st.write('End Date', d2)
    date1_string = d1.strftime('%Y-%m-%d')
    date2_string = d2.strftime('%Y-%m-%d')
    print(date2_string)
    df_window = pd.read_excel('data.xlsx')
    df_window['datetime'] = df_window['datetime'].dt.strftime('%Y-%m-%d')
    matching_rows = df_window[df_window['datetime'] == date1_string]
    matching_row_index = matching_rows.index[0]
    print(matching_row_index)

    # Print the matching rows
    
    
    matching_rows2 = df_window[df_window['datetime'] == date2_string]
    matching_row_index1 = matching_rows2.index[0]
    print(matching_row_index1)
    
    
    # # Print the matching rows
    # print(desired_column2[0])
   
    df_window = df_window.iloc[matching_row_index:matching_row_index1+1,]

    st.write(df_window)
    X = df_window.drop(['_ID_', 'datetime','t'], axis=1)
    y = df_window['trend']
    
    X_test = X.iloc[-1]
    X_test = X_test.to_numpy()
    X_test = X_test.reshape(1,-1)
    print(X_test)
   
    clf_entropy = DecisionTreeClassifier(
            criterion = "entropy", random_state = 42,
            max_depth = 2, min_samples_leaf = 3)
    clf_entropy.fit(X, y)
    y_pred = clf_entropy.predict(X_test)
    
    st.header("__Prediction__")
    if y_pred[0]==1:
        st.subheader("Upward Trend")
    elif y_pred[0]==-1:
        st.subheader("Downward Trend")
    else:
        st.subheader("No Change Notice")
    st.write("As of now the Prediction is for 1.00 AM to 2.00 AM of the last Date that is Enter by user ")
    st.write(d2)
    st.write("This is a Demo. We are working on making prediction hourly basis")
    st.text("As of now overall accuracy of the project is 82 percentage.")



    
    


load_data()
